# Minter Capital Layout

### Разворачивание верстки
Чтобы развернуть проект необходимо установить [Node.js](https://nodejs.org/).


Установить пакетный менеджер [Yarn](https://yarnpkg.com/lang/en/docs/install), либо использовать npm, идущим в пакете с Node.js.


Установить через пакетный менеджер сборщик [Parcel](https://parceljs.org/getting_started.html):
```
yarn global add parcel-bundler
```
либо
```
npm install -g parcel-bundler
```
Склонировать репозиторий.
Через CLI в директории проекта для разработки выполнить:
```
npm run dev
```
для продакшена:
```
npm run build
```
Если используется yarn, то "npm" просто заменяется на "yarn".

### Структура проекта
* **dist** -- Собранный проект
* **img** -- Директория с исходными изображениями, использованными в проекте.
* **js** -- JS файлы, разделенные по функциональным модулям/компонентам.
* **pages** -- HTML блоки/компоненты. Название директории соответствует названию html страницы. 
Например: index.html == /pages/index/...
* **scss** -- CSS стили написанные с использованием препроцессора sass.
/components -- разделение стилей на компоненты (BEM подход).
/utils -- вспомогательные стили (общие классы, пресеты, расширения)
app.scss -- главный файл, в котором подключаются все модули.
fonts.scss -- подключение шрифтов.
variables.scss -- главные переменные проекта.

##### Developed by OneMG Consulting Agency
Sergey Saidefarov <truerosewoodnorman@gmail.com>