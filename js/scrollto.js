require("jquery.scrollto");
$(function() {
	$("a").on("click", function(e) {
		var anchor = $(this).attr("href");
		console.log(anchor);
		if(anchor.charAt(0) != "#") return;
		$("body").scrollTo(anchor, 300, {offset: -$(".navbar").outerHeight()});
		e.preventDefault();
	});
});